package com.sportshop.service;

import java.util.List;

import com.sportshop.dto.CategoryDTO;

public interface ICategoryService {
	List<CategoryDTO> getAll();
}
