package com.sportshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sportshop.dto.ProductDTO;
import com.sportshop.service.IProductService;

@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {
	@Autowired
	IProductService productService;
	@GetMapping("/all")
	public List<ProductDTO> all()
	{
		return productService.getAll();
	}
	@GetMapping("/{id}")
	public ProductDTO get(@PathVariable Long id)
	{
		return productService.get(id);
	}
}
