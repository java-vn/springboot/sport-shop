package com.sportshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sportshop.dto.CategoryDTO;
import com.sportshop.dto.UserDTO;
import com.sportshop.response.AuthResponse;
import com.sportshop.security.MyUserDetails;
import com.sportshop.service.ICategoryService;

@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	ICategoryService categortService;
	@GetMapping("/all")
	public List<CategoryDTO> all()
	{
		return categortService.getAll();
	}
}
